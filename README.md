# Dam

> a barrier constructed to hold back water and raise its level, forming a reservoir used to generate electricity or as a water supply

Initial cluster setup and configuration.

## VM or Bare Metal?

A virtual machines based cluster can be used for fast paced prototyping.

For production workloads a bare-metal cluster would be preferred.

We will setup both to benefit from both stability and fast iteration.

### 3-node Bare Metal HA Kubernetes Cluster

Use the [Coriolis](https://gitlab.com/deposition.cloud/infra/compute/coriolis) Ansible scripts to provision the `live` bare-metal [MicroK8s](https://microk8s.io/) cluster

Then make the nodes aware of each other and ensure High-Availability mode is turned on.

First, let's pick a main node: `node0` at `192.168.0.10` should do.

``` bash
ssh node0
sudo microk8s add-node
```

This will output something like:

``` bash
From the node you wish to join to this cluster, run the following:
microk8s join 192.168.1.10:25000/4b5f66ad586c29e990589c79d0286cb1/783de73c8aaa

Use the '--worker' flag to join a node as a worker not running the control plane, eg:
microk8s join 192.168.1.10:25000/4b5f66ad586c29e990589c79d0286cb1/783de73c8aaa --worker
```

On remaining control nodes, prefix with `sudo`:

``` bash
ssh node1 # node2
sudo microk8s join 192.168.1.10:25000/4b5f66ad586c29e990589c79d0286cb1/783de73c8aaa
```

If you have more than 3 nodes (the minimum required for high-availability), the remaining ones can be configured as workers to keep control plane compute overhead low:

On next remaining worker nodes, prefix with `sudo`:

``` bash
ssh node3 # etc.
sudo microk8s join 192.168.1.10:25000/4b5f66ad586c29e990589c79d0286cb1/783de73c8aaa --worker
```

We can enable a few core services via the MicroK8s addon mechanism. Additional platform services will be managed programatically by [Aurora](https://gitlab.com/deposition.cloud/infra/cluster/aurora) and higher-level projects.

``` bash
$ sudo microk8s status
microk8s is running
high-availability: yes
  datastore master nodes: 192.168.1.10:19001 192.168.1.11:19001 192.168.1.12:19001
  datastore standby nodes: none
addons:
  enabled:
    dns                  # (core) CoreDNS
    ha-cluster           # (core) Configure high availability on the current node
    helm                 # (core) Helm - the package manager for Kubernetes
    helm3                # (core) Helm 3 - the package manager for Kubernetes
    metallb              # (core) Loadbalancer for your Kubernetes cluster
    metrics-server       # (core) K8s Metrics Server for API access to service metrics
    rbac                 # (core) Role-Based Access Control for authorisation
  disabled:
    cert-manager         # (core) Cloud native certificate management
    community            # (core) The community addons repository
    dashboard            # (core) The Kubernetes dashboard
    host-access          # (core) Allow Pods connecting to Host services smoothly
    hostpath-storage     # (core) Storage class; allocates storage from host directory
    ingress              # (core) Ingress controller for external access
    mayastor             # (core) OpenEBS MayaStor
    observability        # (core) A lightweight observability stack for logs, traces and metrics
    prometheus           # (core) Prometheus operator for monitoring and logging
    registry             # (core) Private image registry exposed on localhost:32000
    storage              # (core) Alias to hostpath-storage add-on, deprecated
```

### 1-node or 3-nodes VM Kubernetes Cluster

On a clean [snowflake](https://gitlab.com/deposition.cloud/infra/snowflake)'d Windows 11 machine, use [multipass](https://multipass.run/) to boot up Ubuntu VMs.

``` cmd
# 1 node
multipass list
multipass launch --name vmk8s --mem 12G --disk 50G --cpus 8

# 3 nodes HA
# multipass launch --name vnode0 --mem 8G --disk 30G --cpus 4
# multipass launch --name vnode1 --mem 8G --disk 30G --cpus 4
# multipass launch --name vnode2 --mem 8G --disk 30G --cpus 4
```

``` cmd
multipass shell vmk8s
# multipass shell vnode0
# multipass shell vnode1
# multipass shell vnode2
sudo apt update
sudo apt upgrade --yes
sudo apt autoremove --yes
sudo snap install microk8s --channel=1.25-strict/stable
sudo iptables -P FORWARD ACCEPT

# to uninstall microk8s
sudo snap disable microk8s
sudo snap remove microk8s --purge
```

In `Hyper-V Manager` add an `External Switch` to make the VM visible to the rest of the network (under `Virtual Switch Manager`), then ensure the `Network Adapter` `Virtual switch:` dropdown for `mk8s` vm is set to `External Switch`.

``` sh
sudo microk8s enable rbac
sudo microk8s enable dns:192.168.1.1 # assuming a local DNS server already setup, i.e. on the router
sudo microk8s enable metallb:192.168.1.66-192.168.1.66 # metallb:192.168.1.77-192.168.1.77 # for live
sudo microk8s enable metrics-server
```

## Storage

Make OpenEBS [default storage](https://kubernetes.io/docs/tasks/administer-cluster/change-default-storage-class/#changing-the-default-storageclass), but only after first disabling the current default.

``` bash
kubectl get storageclass
kubectl patch storageclass microk8s-hostpath -p '{"metadata": {"annotations":{"storageclass.kubernetes.io/is-default-class":"false"}}}'
kubectl patch storageclass openebs-jiva-default -p '{"metadata": {"annotations":{"storageclass.kubernetes.io/is-default-class":"true"}}}'
kubectl get storageclass # to check
```

## Local Registry

For building and hosting our own docker images.

On Windows, update Docker Desktop daemon config `Settings > Docker Engine`, as per [MicroK8s > Using the built-in registry](https://microk8s.io/docs/registry-built-in).

``` json
  "insecure-registries": [
    "registry.lan:32000"
  ],
```

Ensure `registry.lan` is configured as a hostname pointing to the active cluster, for instance, in [Turris > Network > Hostnames](http://192.168.1.1/cgi-bin/luci/admin/network/hosts)

This setup allows us to shift between the `live` and `omega` clusters as needed.

Because the tags won't be what the default MicroK8s setup expects, we'll need to treat the local registry as a 3rd party registry and configure it as such, as per [MicroK8s Working with a private registry](https://microk8s.io/docs/registry-private)

Edit `/var/snap/microk8s/current/args/containerd-template.toml` and add the `registry.lan` mirror (note: this could be the same as the `localhost` mirror).

``` toml
      [plugins."io.containerd.grpc.v1.cri".registry.mirrors."registry.lan:32000"]
        endpoint = ["http://registry.lan:32000"]
```

Then restart MicroK8s

``` bash
microk8s stop
microk8s start
```

### Wrong DNS from host daemon?

Try using your own DNS server, ideally an OpenWRT-based router at the edge of your LAN.

Then, on Windows, update Docker Desktop daemon config `Settings > Docker Engine`.

```json
  "dns": [
    "192.168.1.1"
  ],
```

## Troubleshooting

Might need to add `mk8s.lan` to `/var/snap/microk8s/current/certs/csr.conf.template`

``` bash
$ kubectl get nodes
Unable to connect to the server: x509: certificate is valid for kubernetes, kubernetes.default, kubernetes.default.svc, kubernetes.default.svc.cluster, kubernetes.default.svc.cluster.local, not vmk8s.lan
```

Add a line like `DNS.6 = vmk8s.lan`

``` bash
sudo vim /var/snap/microk8s/current/certs/csr.conf.template
```

Wait a few seconds after saving the file for the changes to take effect, then try `kubectl get nodes` to check connectivity.

### Use as GitLab cluster

Add a line like `DNS.7 = deposition.cloud`

Then follow the [setup instructions to add an existing cluster](https://docs.gitlab.com/ee/user/project/clusters/add_remove_clusters.html#add-existing-cluster)

Ensure the public facing domain name is listed:

``` bash
echo | openssl s_client -showcerts -connect deposition.cloud:16443 2>/dev/null | openssl x509 -inform pem -noout -text
```

For that to work, you might need to first configure the firewall rules to point all `16443` traffic to the MicroK8s cluster.

## References

- [MicroK8s Alternative installs (MacOS/Windows 10/Multipass)](https://microk8s.io/docs/install-alternatives)
