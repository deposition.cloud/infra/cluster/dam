import * as pulumi from "@pulumi/pulumi";
import { remote, types } from "@pulumi/command";

const config = new pulumi.Config();
const username = config.require("username");
const microk8sVersion = config.require("microk8sVersion");

const stack = pulumi.getStack();
const org = pulumi.getOrganization();

const nebula = new pulumi.StackReference(`${org}/nebula/${stack}`);

const nebulaOutput = pulumi.unsecret(nebula.getOutput("out"));

export interface Addons {
  rbac: boolean
  dns: string
  metallb: string
  metrics: boolean
  rook: boolean
  ceph: boolean
}

export interface Debug {
  skip: {
    addons: boolean,
    ha: true
  },
  delete: boolean
}

const addons = config.requireObject<Addons>('addons')
const debug = config.requireObject<Debug>('debug')

const outputs = nebulaOutput.apply(output => {
  const clusterInit: {
    dnsCommand?: remote.Command,
    metricsCommand?: remote.Command,
    metallbCommand?: remote.Command,
    rbacCommand?: remote.Command,
    rookCommand?: remote.Command,
    cephCommand?: remote.Command,
    kubeconfigCommand?: remote.Command
  } = {}

  const nodes = output.nodes;

  const install: remote.Command[] = []
  const add: remote.Command[] = []
  const join: remote.Command[] = []

  const main = nodes[0] // arbitrarily choose first node as the main node

  for (let node of nodes) {
    const installCommand = new remote.Command(`install-mk8s-on-${node.vm.name}`, {
      connection: node.provision.connection,
      create: pulumi.interpolate`
        snap list microk8s || sudo snap install microk8s --channel=${microk8sVersion}
        sudo usermod -a -G snap_microk8s ${username}
        mkdir -p ~/.kube
        `,
      delete: debug.delete ? `sudo snap remove microk8s` : ''
    });

    install.push(installCommand)

    if (node === main) {
      clusterInit.kubeconfigCommand = new remote.Command(`kubeconfig`, {
        connection: main.provision.connection,
        create: `sudo microk8s config`
      }, {
        parent: installCommand,
        dependsOn: installCommand
      });

      if (!debug.skip.addons) {
        const dependsOn: pulumi.Input<pulumi.Resource>[] = []

        if (addons.dns) {
          clusterInit.dnsCommand = new remote.Command(`dns`, {
            connection: main.provision.connection,
            create: `sudo microk8s enable dns:${addons.dns}`,
            delete: debug.delete ? `sudo microk8s disable dns` : ''
          }, {
            parent: installCommand,
            dependsOn: installCommand
          });

          dependsOn.push(clusterInit.dnsCommand)
        }

        if (addons.rbac) {
          clusterInit.rbacCommand = new remote.Command(`rbac`, {
            connection: main.provision.connection,
            create: `sudo microk8s enable rbac`,
            delete: debug.delete ? `sudo microk8s disable rbac` : ''
          }, {
            parent: installCommand,
            dependsOn: installCommand
          });

          dependsOn.push(clusterInit.rbacCommand)

          if (addons.metrics) {
            clusterInit.metricsCommand = new remote.Command(`metrics-server`, {
              connection: main.provision.connection,
              create: `sudo microk8s enable metrics-server`,
              delete: debug.delete ? `sudo microk8s disable metrics-server` : ''
            }, {
              parent: clusterInit.rbacCommand,
              dependsOn: clusterInit.rbacCommand
            });

            dependsOn.push(clusterInit.metricsCommand)
          }
        }

        if (addons.metallb) {
          clusterInit.metallbCommand = new remote.Command(`metallb`, {
            connection: main.provision.connection,
            create: `sudo microk8s enable metallb:${addons.metallb}`,
            delete: debug.delete ? `sudo microk8s disable metallb` : ''
          }, {
            parent: installCommand,
            dependsOn: installCommand
          });

          dependsOn.push(clusterInit.metallbCommand)
        }

        // if (addons.rook) {
        //   clusterInit.rookCommand = new remote.Command(`rook`, {
        //     connection: main.provision.connection,
        //     create: `sudo microk8s enable rook-ceph`,
        //     delete: debug.delete ? `sudo microk8s disable rook-ceph` : ''
        //   }, {
        //     parent: installCommand,
        //     dependsOn: installCommand
        //   });

        //   dependsOn.push(clusterInit.rookCommand)

        //   if (addons.ceph) {
        //     const writeCephConfigCommand = new remote.Command("ceph-config", {
        //       connection: main.provision.connection,
        //       create: pulumi.interpolate`mkdir -p $HOME/.ceph && echo "${output.ceph.config}" >> $HOME/.ceph/ceph.conf`,
        //     }, {
        //       parent: clusterInit.rookCommand,
        //       dependsOn: clusterInit.rookCommand
        //     });

        //     const writeCephKeyringCommand = new remote.Command("ceph-keyring", {
        //       connection: main.provision.connection,
        //       create: pulumi.interpolate`mkdir -p $HOME/.ceph && echo "${output.ceph.keyring}" >> $HOME/.ceph/ceph.keyring`,
        //     }, {
        //       parent: clusterInit.rookCommand,
        //       dependsOn: clusterInit.rookCommand
        //     });

        //     clusterInit.cephCommand = new remote.Command(`ceph`, {
        //       connection: main.provision.connection,
        //       create: `sudo microk8s connect-external-ceph --ceph-conf $HOME/.ceph/ceph.conf --keyring $HOME/.ceph/ceph.keyring --rbd-pool ${addons.ceph}`,
        //       delete: debug.delete ? `sudo microk8s disable metrics-server` : ''
        //     }, {
        //       parent: clusterInit.rookCommand,
        //       dependsOn: [clusterInit.rookCommand, writeCephConfigCommand, writeCephKeyringCommand]
        //     });

        //     dependsOn.push(clusterInit.cephCommand)
        //   }
        // }

        const rebootCommand = new remote.Command(`reboot-${main.vm.name}`, {
          connection: node.provision.connection,
          create: pulumi.interpolate`
            sudo dhclient -r
            sudo dhclient
            echo "sleep 5 ; sudo shutdown -r now" | at now`,
        }, {
          parent: installCommand,
          dependsOn: [installCommand, ...dependsOn]
        });
      }
    } else if (!debug.skip.ha) { // Add extra nodes to the cluster
      const addCommand = new remote.Command(`add-node-${node.vm.name}-to-${main.vm.name}`, {
        connection: main.provision.connection,
        create: `sudo microk8s add-node`
      }, {
        parent: installCommand,
        dependsOn: installCommand
      });

      add.push(addCommand)

      const joinCommand = addCommand.stdout.apply(add => {
        const joinLine = add.split('\n')[1].trim();

        const joinCommand = new remote.Command(`join-${node.vm.name}`, {
          connection: node.provision.connection,
          create: `sudo ${joinLine}`,
          delete: debug.delete ? `sudo microk8s kubectl drain ${node.vm.name} && sudo mircrok8s leave --ignore-daemonsets` : ''
        }, {
          parent: addCommand,
          dependsOn: addCommand
        });

        const rebootCommand = new remote.Command(`${node.vm.name}-reboot`, {
          connection: node.provision.connection,
          create: pulumi.interpolate`
            sudo dhclient -r
            sudo dhclient
            echo "sleep 5 ; sudo shutdown -r now" | at now`,
        }, {
          parent: joinCommand,
          dependsOn: joinCommand
        });

        return joinCommand
      })
      
      joinCommand.apply(command => join.push(command))
    }
  }

  return {
    install,
    add,
    join,
    ...clusterInit,
    kubeconfig: clusterInit?.kubeconfigCommand?.stdout
  }
});

export const out = outputs
